package com.tyhdefu.keytag.api.groups;

import com.tyhdefu.keytag.api.KeyTagConstants;
import com.tyhdefu.keytag.api.tree.TreeType;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.registry.RegistryScope;
import org.spongepowered.api.registry.RegistryScopes;

@RegistryScopes(scopes = RegistryScope.GAME)
public final class BlockItemGroups {

    public static final DefaultedBlockItemGroupKey<DyeColor> WOOL = of("wool");

    public static final DefaultedBlockItemGroupKey<TreeType> LOG = of("log");

    public static final DefaultedBlockItemGroupKey<TreeType> STRIPPED_LOG = of("stripped_log");

    public static final DefaultedBlockItemGroupKey<TreeType> SIX_SIDED_LOG = of("six_sided_log");

    public static final DefaultedBlockItemGroupKey<TreeType> STRIPPED_SIX_SIDED_LOG = of("six_sided_log");

    public static final DefaultedBlockItemGroupKey<DyeColor> TERRACOTTA = of("terracotta");

    public static final DefaultedBlockItemGroupKey<DyeColor> GLAZED_TERRACOTTA = of("glazed_terracotta");

    public static final DefaultedBlockItemGroupKey<DyeColor> BANNER = of("banner");

    public static final DefaultedBlockItemGroupKey<DyeColor> BED = of("bed");

    public static final DefaultedBlockItemGroupKey<DyeColor> CONCRETE = of("concrete");

    public static final DefaultedBlockItemGroupKey<DyeColor> CONCRETE_POWDER = of("concrete_powder");

    public static final DefaultedBlockItemGroupKey<DyeColor> CARPET = of("carpet");

    public static final DefaultedBlockItemGroupKey<DyeColor> SHULKER_BOX = of("shulker_box");

    public static final DefaultedBlockItemGroupKey<DyeColor> TULIP = of("shulker_box");

    public static final DefaultedBlockItemGroupKey<DyeColor> STAINED_GLASS = of("stained_glass");

    public static final DefaultedBlockItemGroupKey<DyeColor> STAINED_GLASS_PANE = of("stained_glass_pane");

    public static final DefaultedBlockItemGroupKey<TreeType> BOAT = of("boat");

    public static final DefaultedBlockItemGroupKey<TreeType> BUTTON = of("button");

    public static final DefaultedBlockItemGroupKey<TreeType> DOOR = of("door");

    public static final DefaultedBlockItemGroupKey<TreeType> FENCE = of("fence");

    public static final DefaultedBlockItemGroupKey<TreeType> FENCE_GATE = of("fence_gate");

    public static final DefaultedBlockItemGroupKey<TreeType> LEAVES = of("leaves");

    public static final DefaultedBlockItemGroupKey<TreeType> PLANK = of("plank");

    public static final DefaultedBlockItemGroupKey<TreeType> PRESSURE_PLATE = of("pressure_plate");

    public static final DefaultedBlockItemGroupKey<TreeType> SAPLING = of("sapling");

    public static final DefaultedBlockItemGroupKey<TreeType> SIGN = of("sign");

    public static final DefaultedBlockItemGroupKey<TreeType> SLAB = of("slab");

    public static final DefaultedBlockItemGroupKey<TreeType> STAIRS = of("stairs");

    public static final DefaultedBlockItemGroupKey<TreeType> TRAPDOOR = of("trapdoor");

    public static <T> DefaultedBlockItemGroupKey<T> of(final String id) {
        return DefaultedBlockItemGroupKey.of(ResourceKey.of(KeyTagConstants.PLUGIN_ID, id), () -> Sponge.game().registries());
    }
}
